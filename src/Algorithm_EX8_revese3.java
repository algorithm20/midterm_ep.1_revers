import java.io.File;
import java.io.IOException;
import java.util.*;

public class Algorithm_EX8_revese3 {
	public static void main(String[] args) throws IOException {
		LinkedList<Integer> stack = new LinkedList<>();
		Scanner kb = new Scanner(System.in);
		String namecheck = kb.next();
		File file = new File("C:\\code\\Exam8\\input_ep1.txt");
		Scanner inputData = new Scanner(checkInput(namecheck, file));

		addDataToStack(stack, inputData);
		System.out.print(stack);
	}

	static File checkInput(String namecheck, File file) throws IOException {
		if (namecheck.equals("1.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep1.txt");
		} else if (namecheck.equals("2.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep2.txt");
		} else if (namecheck.equals("3.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep3.txt");
		} else if (namecheck.equals("4.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep4.txt");
		} else if (namecheck.equals("5.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep5.txt");
		} else if (namecheck.equals("6.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep6.txt");
		} else if (namecheck.equals("7.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep7.txt");
		} else if (namecheck.equals("8.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep8.txt");
		} else if (namecheck.equals("9.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep9.txt");
		} else if (namecheck.equals("10.in")) {
			return file = new File("C:\\code\\Exam8\\input_ep10.txt");
		}

		return file;
	}

	static LinkedList<Integer> addDataToStack(LinkedList<Integer> stack, Scanner in) {
		while (in.hasNext()) {
			stack.push(in.nextInt());
		}
		return stack;
	}

}
